## Akka graph DB
### How to use
requirements   
* sbt   
* python 3.5+ with requests module for testing   

To run unittests execute ```sbt test```   
To start application ```sbt run```   
To populate database by random data run ```python3 case.py``` (from test folder)   
To execute test query run ```python3 query.py``` (this file needs to be adjusted)   

Application provides WEB API which is available on port 8080   


## API documentation
### Create Entity
 
Endpoint: ```POST http://localhost:8080/v1/add```   
Body:   
```   
{   
  "label": <type of entity: str>   
}   
```   
####Example   
```   
POST http://localhost:8080/v1/add   
Content-Type: application/json   
   
{   
  "label": "PERSON"   
}   
```   
Response:   
```   
HTTP/1.1 200 OK   

{   
  "nodeId": "<id of created node:str>"   
}   
```   


### Add attribute to the existing entity   
Endpoint: ```POST http://localhost:8080/v1/attr```     
Body:   
```   
{   
  "nodeId": "<unique id: str>",   
  "key": "<name of attribute: str>",   
  "value": <attribute: str|double|bool|list>   
}   
```   
####Example   
```   
POST http://localhost:8080/v1/attr   
Content-Type: application/json   
   
{
  "nodeId": "person-0",   
  "key": "age",   
  "value": 35   
}   
```   
   
###Output content of node   
Endpoint: ```GET http://localhost:8080/v1/node/<nodeId:str>```    
  
####Example 
```   
GET http://localhost:8080/v1/node/PERSON-0   
Content-Type: application/json   
```   
   
###Query database   
Endpoint: ```POST http://localhost:8080/v1/eval```     
Body:   
```   
{   
  "label": "<type of node: str>",   
  "eval": <condition>   
}   
```   
Condition has quite complex api, description will be added later   
####Example    
List all persons employed by business located in Sydney:   
```   
POST http://localhost:8080/v1/eval
Content-Type: application/json

{
  "label": "PERSON",
  "eval": {
                  "type": "OUT",
                  "kind": "EMPLOYED_BY",
                  "eval": {
                      "type": "AND",
                      "eval1": {
                          "type": "LABEL",
                          "value": "BUSINESS"
                      },
                      "eval2": {
                          "type": "EQ",
                          "key": "location",
                          "value": "London"
                      }
                  }
              }
}
```   

###Connect nodes   
Endpoint: ```POST http://localhost:8080/v1/connect```    
Body:   
```
{
    "from": <source nodeId: str>,
    "to": <destination nodeId: str>,
    "relation": <relation type: str>
}
```
####Example   
```   
POST http://localhost:8080/v1/connect
Content-Type: application/json

{
    "from": "PERSON-1",
    "to": "PERSON-2",
    "relation": "FRIEND"
}
```   

###Delete attribute from node   
Endpoint: ```POST http://localhost:8080/v1/delete```    
Body:   
```   
{
 
    "nodeId": <nodeId:str>,
    "key": <attribute name: str>
}
```   
####Example
```   
POST http://localhost:8080/v1/delete   
Content-Type: application/json

{
    "nodeId": "PERSON-1",
    "key": "age"
}
```   
   
###Update all the nodes which meet condition   
Endpoint: ```POST http://localhost:8080/v1/update```   
Body:   
```   
{
    "label": <node type: str>,
    "key": <attribute: str>,
    "expression": <formula>,
    "condition": <logical condition>
}
```   
Example of expression: "(((key1 + 45) * key2) - 48) + (-1)" it will be parsed substituted and calculated. Supports only numeric attributes     
Condition has similar to query format (see above)   
####Example   
```
POST http://localhost:8080/v1/update   
Content-Type: application/json   

{
    "label": "CONTRACT",
    "key": "salary,
    "expression": "salary * 1.1",
    "condition": {
                         "type": "AND",
                         "eval1": {
                                 "type": "AND",
                                 "eval1": {
                                     "type": "IN",
                                     "kind": "EMPLOYED_BY",
                                     "eval": {
                                         "type": "LABEL",
                                         "value": "PERSON"
                                     }
                                 },
                                 "eval2": {
                                     "type": "OUT",
                                     "kind": "EMPLOYED_BY",
                                     "eval": {
                                         "type": "OUT",
                                         "kind": "EMPLOYED_BY",
                                         "eval": {
                                             "type": "LABEL",
                                             "value": "BUSINESS"
                                         }
                                     }
                                 }
                             },
                         "eval2": {
                             "type": "EQ",
                             "key": "position",
                             "value": "developer"
                         }
                }
}
```
