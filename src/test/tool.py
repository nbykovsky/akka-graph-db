import requests
import json


class GraphApi:

    def ifprint(self, line):
        if self._prt:
            print(line)

    def __line(self):
        self.ifprint("______________________________________________________")

    def __init__(self, uri, prt=False):
        self._uri = uri
        self._prt = prt

    def add_node(self, label):
        payload = {
            "label": label
        }
        response = requests.post("%s/v1/add" % self._uri, json=payload)
        if response.status_code >= 300:
            raise ConnectionError(response.text)
        node_id = response.json()["nodeId"]
        self.ifprint("ADDED NODE\nlabel: %s\nnode_id %s" % (label, node_id))
        self.__line()

        return node_id

    def add_attribute(self, node_id, key, value):
        payload = {
            "nodeId": node_id,
            "key": key,
            "value": value
        }
        response = requests.post("%s/v1/attr" % self._uri, json=payload)
        if response.status_code >= 300:
            raise ConnectionError(response.text)
        self.ifprint("ADDED ATTRIBUTE")
        self.ifprint("node: %s\n\nkey: %s\nvalue: %s"% (node_id, key, value))
        self.__line()

    def get_node_info(self, node_id):
        self.ifprint("REQUESTED NODE DATA")
        self.ifprint("nodeId: %s\n" % node_id)
        response = requests.get("%s/v1/node/%s" % (self._uri, node_id))

        if response.status_code >= 300:
            raise ConnectionError(response.text)

        for k,v in response.json().items():
            self.ifprint("%s -> %s" % (k, v))
        self.__line()
        return response.json()

    def eval_command(self, label, command):
        payload = {
            "label": label,
            "eval": command
        }
        self.ifprint("SENDING COMMAND")
        self.ifprint("label: %s" % label)
        self.ifprint("command: \n %s \n" % json.dumps(command, indent=4))
        response = requests.post("%s/v1/eval" % self._uri, json=payload)
        if response.status_code >= 300:
            raise ConnectionError(response.text)
        self.ifprint("result: ")
        self.ifprint(json.dumps(response.json(), indent=4))
        self.__line()
        return response.json()

    def connect_nodes(self, node_from, node_to, relation):
        payload = {
            "from": node_from,
            "to": node_to,
            "relation": relation
        }
        self.ifprint("CONNECTING NODES")
        self.ifprint("%s --> %s" % (node_from, node_to))
        self.ifprint("relation type: %s" % relation)
        response = requests.post("%s/v1/connect" % self._uri, json=payload)
        if response.status_code >= 300:
            raise ConnectionError(response.text)
        self.__line()

    def delete_attr(self, node_id, key):
        payload = {
            "nodeId": node_id,
            "key": key
        }
        self.ifprint("DELETING ATTRIBUTE")
        self.ifprint("nodeId: %s\nkey: %s" % (node_id, key))
        response = requests.post("%s/v1/delete" % self._uri, json=payload)
        if response.status_code >= 300:
            raise ConnectionError(response.text)
        self.__line()

    def update(self, label, key, exp, cond):
        payload = {
            "label": label,
            "key": key,
            "expression": exp,
            "condition": cond
        }
        self.ifprint("UPDATING NODES")
        self.ifprint("label: %s" % label)
        self.ifprint("key: %s" % key)
        self.ifprint("expression: %s" % exp)
        self.ifprint("condition: %s" % cond)
        response = requests.post("%s/v1/update" % self._uri, json=payload)
        if response.status_code >= 300:
            raise ConnectionError(response.text)
        self.__line()

    def create_node(self, label, **kwargs):
        node_id = self.add_node(label)
        for k, v in kwargs.items():
            self.add_attribute(node_id, k, v)
        return node_id

