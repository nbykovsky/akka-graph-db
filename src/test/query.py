from tool import GraphApi
import json


def main():
    api = GraphApi("http://localhost:8080", prt=True)

    # list all persons
    api.eval_command("PERSON", {
        "type": "LABEL",
        "value": "PERSON",
    })

    # list all friend of person with name "Mike Abbott"
    api.eval_command("PERSON", {
        "type": "OUT",
        "kind": "FRIEND",
        "eval": {
            "type": "EQ",
            "key": "name",
            "value": "Floy Abell"
        }
    })

    # list all employed persons
    api.eval_command("PERSON", {
        "type": "OUT",
        "kind": "EMPLOYED_BY",
        "eval": True
    })

    # list all persons employed by business located in Sydney
    api.eval_command("PERSON", {
        "type": "OUT",
        "kind": "EMPLOYED_BY",
        "eval": {
            "type": "AND",
            "eval1": {
                "type": "LABEL",
                "value": "BUSINESS"
            },
            "eval2": {
                "type": "EQ",
                "key": "location",
                "value": "Moscow"
            }
        }
    })
    # increase salary to person who employed by company "Company 5" and holding position "developer"
    query = {
        "type": "AND",
        "eval1": {
                "type": "AND",
                "eval1": {
                    "type": "IN",
                    "kind": "EMPLOYED_BY",
                    "eval": {
                        "type": "LABEL",
                        "value": "PERSON"
                    }
                },
                "eval2": {
                    "type": "OUT",
                    "kind": "EMPLOYED_BY",
                    "eval": {
                        "type": "OUT",
                        "kind": "EMPLOYED_BY",
                        "eval": {
                            "type": "LABEL",
                            "value": "BUSINESS"
                        }
                    }
                }
            },
        "eval2": {
            "type": "EQ",
            "key": "position",
            "value": "developer"
        }
    }
    api.eval_command("CONTRACT", query)
    api.update("CONTRACT", "salary", "salary * 1.1", query)
    api.eval_command("CONTRACT", query)


if __name__ == '__main__':
    main()

