
from tool import GraphApi
import random


positions_names = ["developer", "tester", "cleaner", "hr", "farmer", "cook"]
titles = ["VP", "AVP", "Manager", "Assistant Manager", "Junior"]
names = ["Jonh", "Mike", "Tanisha","Homer","Matthew","Monroe", "Stacey","Nakesha",
         "Duncan","India","Tressa","Floy","Brock","Shonta","Maryann","Russell","Golden",
         "Adalberto""Allen","Macy","Leticia","Renae"]
surnames = ["Aaron","Abbott","Abel","Abell","Abernathy","Abney","Abraham","Abrams",
            "Abreu","Acevedo","Acker",]

locations = ["Sydney", "Singapore", "London", "Moscow", "Minsk"]


random.seed(101)


def main():
    api = GraphApi("http://localhost:8080")

    # creating 100 nodes of type PERSON
    persons = []
    for i in range(100):
        age = random.randint(18,101)
        name = "%s %s" % (random.choice(names),random.choice(surnames))
        persons.append(api.create_node("PERSON", name=name, age=age))

    # creating 30 nodes of type BUSINESS located in 5 different locations
    business = []
    for i in range(5):
        location = random.choice(locations)
        business.append(api.create_node("BUSINESS", name="Company %s" % i, location=location))

    # creating 50 random contracts
    contracts = []
    for i in range(50):
        salary = random.randint(10000,100000)
        contracts.append(api.create_node("CONTRACT", date="date %s" % i, salary=salary, position=random.choice(positions_names)))

    # creating 50 positions
    positions = []
    for i in range(50):
        title = random.choice(titles)
        positions.append(api.create_node("POSITION", title=title))

    # Randomly add FRIEND relation between persons
    for i in range(50):
        f = random.choice(persons)
        t = random.choice(persons)
        api.connect_nodes(f, t, "FRIEND")

    # Randomly add EMPLOYED_BY relation between person and business
    for i in range(50):
        f = random.choice(persons)
        t = random.choice(business)
        api.connect_nodes(f, t, "EMPLOYED_BY")

    # Randomly add EMPLOYED_BY relation between person and contract
    for i in range(50):
        f = random.choice(persons)
        t = random.choice(contracts)
        api.connect_nodes(f, t, "EMPLOYED_BY")

    # Randomly add EMPLOYED_BY relation between contract and position
    for i in range(50):
        f = random.choice(contracts)
        t = random.choice(positions)
        api.connect_nodes(f, t, "EMPLOYED_BY")

    # Randomly add EMPLOYED_BY relation between position and business
    for i in range(50):
        f = random.choice(positions)
        t = random.choice(business)
        api.connect_nodes(f, t, "EMPLOYED_BY")


if __name__ == '__main__':
    main()
