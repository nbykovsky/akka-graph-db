import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class TestCollector extends TestKit(ActorSystem("Collector", ConfigFactory.load(config.testingConf))) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val graphRegionMock: TestProbe = TestProbe()
  val labelRegionMock: TestProbe = TestProbe()

  val collectorActor: ActorRef = system.actorOf(Props(new Collector() {
    override lazy val graphRegion: ActorRef = graphRegionMock.testActor
    override lazy val labelRegion: ActorRef = labelRegionMock.testActor
    override val remotePath = "remote/path"
  }) , "collector")

  "Collector Actor" must {
    "Accept query and reply with empty result" in {
      collectorActor ! Collector.PostQuery(Node.True, "test")
      labelRegionMock.expectMsg(NodeManager.GetHead("test"))
      labelRegionMock.reply(NodeManager.NodeResp("test-1"))
      graphRegionMock.expectMsg(Node.InitEvalCmd("test-1", "remote/path", Node.True))
      graphRegionMock.reply(common.Ok)
      expectMsg(Collector.Result(List()))
    }

    "Accept query and reply with non empty result" in {
      collectorActor ! Collector.PostQuery(Node.True, "test")
      labelRegionMock.expectMsg(NodeManager.GetHead("test"))
      labelRegionMock.reply(NodeManager.NodeResp("test-1"))
      graphRegionMock.expectMsg(Node.InitEvalCmd("test-1", "remote/path", Node.True))
      graphRegionMock.reply(common.Ok)
      collectorActor ! Node.InitEvalResp(Map("a"->Node.AttrString("b")),"test-1", "test")
      expectMsg(Collector.Result(List(Node.InitEvalResp(Map("a"->Node.AttrString("b")),"test-1", "test"))))
    }

  }

}
