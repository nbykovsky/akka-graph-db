import Routes.attrValueFormat
import org.scalatest._
import spray.json.{JsBoolean, JsObject, JsString}
import spray.json._


class TestRoutes extends FunSuite {

  test("Parse bool expressions") {
    assert(Routes.evalReader.read(JsBoolean(true)) == Node.True)
    assert(Routes.evalReader.read(JsBoolean(false)) == Node.False)
  }

  test("Parse EQ expressions") {
    assert(Routes.evalReader.read(JsObject("type"->JsString("EQ"), "key"->JsString("a"), "value"->JsString("b"))) ==
      Node.Eq("a", Node.AttrString("b")))
  }

  test("Parse OR expressions") {
    assert(Routes.evalReader.read(JsObject("type"->JsString("OR"), "eval1"->JsBoolean(true), "eval2"->JsBoolean(false))) ==
      Node.Or(Node.True, Node.False))
  }

  test("Parse AND expressions") {
    assert(Routes.evalReader.read(JsObject("type"->JsString("AND"), "eval1"->JsBoolean(true), "eval2"->JsBoolean(false))) ==
      Node.And(Node.True, Node.False))
  }

  test("Parse NOT expressions") {
    assert(Routes.evalReader.read(JsObject("type"->JsString("NOT"), "eval"->JsBoolean(true))) ==
      Node.Not(Node.True))
  }

  test("Parse IN relation") {
    assert(Routes.evalReader.read(JsObject("type"->JsString("IN"), "kind"->JsString("friend"), "eval"->JsBoolean(true))) ==
      Node.In("friend", Node.True))
  }

  test("Parse OUT relation") {
    assert(Routes.evalReader.read(JsObject("type"->JsString("OUT"), "kind"->JsString("friend"), "eval"->JsBoolean(false))) ==
      Node.Out("friend", Node.False))
  }

  test("Parse complex JSON"){
    val jsn =
      """
        {
        "type": "OR",
        "eval1": {
            "type": "NOT",
            "eval": true
          },
        "eval2": {
            "type": "IN",
            "kind": "friend",
            "eval": {
              "type": "EQ",
              "key": "a",
              "value": "b"
            }
          }
        }
      """.parseJson

    val target = Node.Or(Node.Not(Node.True), Node.In("friend", Node.Eq("a", Node.AttrString("b"))))
    assert(Routes.evalReader.read(jsn) == target)

  }

}
