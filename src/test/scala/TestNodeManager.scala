import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class TestNodeManager extends TestKit(ActorSystem("NodeManager", ConfigFactory.load(config.testingConf))) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val graphRegionMock: TestProbe = TestProbe()

  val nodeManagerActor: ActorRef = system.actorOf(Props(new NodeManager() {
    override lazy val graphRegion: ActorRef = graphRegionMock.testActor
    override val persistenceId: String = "label-test"
  }) , "some-label")


  "NodeManager actor" must {
    "Return Err if node doesn't exists" in {
      nodeManagerActor ! NodeManager.GetHead("some-label")
      expectMsg(common.Err)
    }

    "correctly create first node of given type" in {
      nodeManagerActor ! NodeManager.AddNodeCmd("test")
      graphRegionMock.expectMsg(Node.SetLabelCmd("test-0", "test"))
      graphRegionMock.reply(common.Ok)
      graphRegionMock.expectNoMessage()
      expectMsg(NodeManager.NodeResp("test-0"))
    }

    "return head when one node exists" in {
      nodeManagerActor ! NodeManager.GetHead("test")
      expectMsg(NodeManager.NodeResp("test-0"))
    }

    "correctly create second node of given type" in {
      nodeManagerActor ! NodeManager.AddNodeCmd("test")
      graphRegionMock.expectMsg(Node.SetLabelCmd("test-1", "test"))
      graphRegionMock.reply(common.Ok)
      graphRegionMock.expectMsg(Node.SetNextNodeCmd("test-1", "test-0"))
      graphRegionMock.reply(common.Ok)
      graphRegionMock.expectNoMessage()
      expectMsg(NodeManager.NodeResp("test-1"))
    }

    "return head when two nodes exists" in {
      nodeManagerActor ! NodeManager.GetHead("test")
      expectMsg(NodeManager.NodeResp("test-1"))
    }

    "recover state after it was killed" in {
      system.stop(nodeManagerActor)
      val otherActor: ActorRef = system.actorOf(Props(new NodeManager() {
        override lazy val graphRegion: ActorRef = graphRegionMock.testActor
        override val persistenceId: String = "label-test"
      }) , "otherNode")
      otherActor ! NodeManager.GetHead("test")
      expectMsg(NodeManager.NodeResp("test-1"))

      otherActor ! NodeManager.AddNodeCmd("test")
      graphRegionMock.expectMsg(Node.SetLabelCmd("test-2", "test"))
      graphRegionMock.reply(common.Ok)
      graphRegionMock.expectMsg(Node.SetNextNodeCmd("test-2", "test-1"))
      graphRegionMock.reply(common.Ok)
      expectMsg(NodeManager.NodeResp("test-2"))

    }

  }

}
