import org.scalatest.FunSuite

class TestParsers  extends FunSuite {

  test("parse value expression") {
    assert(parsers.exp.parse("13") == Right(Node.Val(13)))
  }
  test("parse float value expression") {
    assert(parsers.exp.parse("13.5") == Right(Node.Val(13.5)))
  }
  test("parse float 0 value expression") {
    assert(parsers.exp.parse("-0.5") == Right(Node.Val(-0.5)))
  }
  test("parse variable expression") {
    assert(parsers.exp.parse("qwe") == Right(Node.Var("qwe")))
  }
  test("parse add expression") {
    assert(parsers.exp.parse("1 + r") == Right(Node.Add(Node.Val(1), Node.Var("r"))))
    assert(parsers.exp.parse("1+r") == Right(Node.Add(Node.Val(1), Node.Var("r"))))
  }

  test("parse prod expression") {
    assert(parsers.exp.parse("1 * r") == Right(Node.Prod(Node.Val(1), Node.Var("r"))))
    assert(parsers.exp.parse("1*r") == Right(Node.Prod(Node.Val(1), Node.Var("r"))))
  }

  test("parse subtract expression") {
    assert(parsers.exp.parse("1 - r") == Right(Node.Sub(Node.Val(1), Node.Var("r"))))
    assert(parsers.exp.parse("1-r") == Right(Node.Sub(Node.Val(1), Node.Var("r"))))
  }

  test("parse complex expression") {
    assert(parsers.exp.parse("(1 + 1) * (a + (b * (c + d)))") == Right(Node.Prod(Node.Add(Node.Val(1),Node.Val(1)),Node.Add(Node.Var("a"),Node.Prod(Node.Var("b"),Node.Add(Node.Var("c"),Node.Var("d")))))))
  }

  test("parse expression with negative numbers") {
    assert(parsers.exp.parse("-1 - r") == Right(Node.Sub(Node.Val(-1), Node.Var("r"))))
    assert(parsers.exp.parse("-1 - (-5 + 5)") == Right(Node.Sub(Node.Val(-1), Node.Add(Node.Val(-5), Node.Val(5)))))
  }

  test("evaluate value") {
    assert(parsers.evaluate(Node.Val(10), Map()).contains(10))
  }

  test("evaluate existing variable") {
    assert(parsers.evaluate(Node.Var("x"), Map("x"->Node.AttrNumber(10))).contains(10))
  }

  test("evaluate non existing variable") {
    assert(parsers.evaluate(Node.Var("x"), Map()).isEmpty)
  }

  test("evaluate add") {
    assert(parsers.evaluate(Node.Add(Node.Val(10), Node.Val(5)), Map()).contains(15))
  }

  test("evaluate prod") {
    assert(parsers.evaluate(Node.Prod(Node.Val(10), Node.Val(5)), Map()).contains(50))
  }

  test("evaluate sub") {
    assert(parsers.evaluate(Node.Sub(Node.Val(10), Node.Val(5)), Map()).contains(5))
  }

  test("evaluate long valid expression") {
    val exp = Node.Add(Node.Prod(Node.Val(10), Node.Var("a")), Node.Var("c"))
    assert(parsers.evaluate(exp, Map("a"->Node.AttrNumber(2), "c"->Node.AttrNumber(5))).contains(25))
  }

  test("evaluate long non valid expression") {
    val exp = Node.Add(Node.Prod(Node.Val(10), Node.Var("a")), Node.Var("d"))
    assert(parsers.evaluate(exp, Map("a"->Node.AttrNumber(2), "c"->Node.AttrNumber(5))).isEmpty)
  }
}
