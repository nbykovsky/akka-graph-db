import akka.actor.{ActorRef, ActorSystem, Address, PoisonPill, Props}
import akka.testkit.{ImplicitSender, TestActor, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}


class TestNode extends TestKit(ActorSystem("Node", ConfigFactory.load(config.testingConf))) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll{

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val graphRegionMock: TestProbe = TestProbe()


  val probe: TestProbe = TestProbe()
  val remoteAddr: Address = common.RemoteAddressExtension(system).address
  val remotePath: String = probe.testActor.path.toStringWithAddress(remoteAddr)


  val nodeActor: ActorRef = system.actorOf(Props(new Node() {
    override lazy val graphRegion: ActorRef = graphRegionMock.testActor
  }) , "nodeId")

  "Node actor" must {

    "response NodeDoesntExists to GetAllAttrCmd" in {
      nodeActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeDoesntExists)
    }

    "response Ok to SetLabelCmd command" in {
      nodeActor ! Node.SetLabelCmd("nodeId", "nodeLabel")
      expectMsg(common.Ok)
    }

    "response GetAllAttrResp to GetAllAttrCmd" in {
      nodeActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeData(Map(), "nodeLabel", Map(), Map(), None))
    }

    "correctly set next node first time" in {
      nodeActor ! Node.SetNextNodeCmd("nodeId", "nextNodeId")
      expectMsg(common.Ok)
      nodeActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeData(Map(), "nodeLabel", Map(), Map(), Some("nextNodeId")))
    }

    "return error if trying set next node second time" in {
      nodeActor ! Node.SetNextNodeCmd("nodeId", "nextNodeId")
      expectMsg(common.Err)
    }

    "correctly set String attribute" in {
      nodeActor ! Node.AddAttrCmd("nodeId", "key", Node.AttrString("value"))
      expectMsg(common.Ok)
      nodeActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeData(Map("key"->Node.AttrString("value")), "nodeLabel", Map(), Map(), Some("nextNodeId")))
    }

    "correctly add 2 inbound relations of same type" in {
      nodeActor ! Node.AddInRelCmd("nodeId", "a", "some-relation")
      expectMsg(common.Ok)
      nodeActor ! Node.AddInRelCmd("nodeId", "b", "some-relation")
      expectMsg(common.Ok)
      nodeActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeData(Map("key"->Node.AttrString("value")), "nodeLabel", Map("some-relation"->List("b", "a")), Map(), Some("nextNodeId")))
    }

    "correctly add 2 outbound relations of same type" in {
      nodeActor ! Node.AddOutRelCmd("nodeId", "a", "some-relation")
      expectMsg(common.Ok)
      nodeActor ! Node.AddOutRelCmd("nodeId", "b", "some-relation")
      expectMsg(common.Ok)
      nodeActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeData(Map("key"->Node.AttrString("value")), "nodeLabel", Map("some-relation"->List("b", "a")), Map("some-relation"->List("b", "a")), Some("nextNodeId")))
    }

    "correctly accept and evaluate InitEvalCmd with dummy predicate" in {
      nodeActor ! Node.InitEvalCmd("nodeId", remotePath, Node.True)
      graphRegionMock.expectMsg(Node.InitEvalCmd("nextNodeId", remotePath, Node.True))
      expectMsg(common.Ok)
      probe.expectMsg(Node.InitEvalResp(Map("key"->Node.AttrString("value")), "nodeId", "nodeLabel"))
    }

    "correctly evaluate positive Eq command" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.Eq("key", Node.AttrString("value")))
      expectMsg(Node.EvalResp(true))
    }

    "correctly evaluate negative Eq command" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.Eq("key", Node.AttrString("other value")))
      expectMsg(Node.EvalResp(false))
      nodeActor ! Node.EvalCmd("nodeId", Node.Eq("not exists", Node.AttrString("other value")))
      expectMsg(Node.EvalResp(false))
    }

    "correctly evaluate not command" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.Not(Node.True))
      expectMsg(Node.EvalResp(false))

      nodeActor ! Node.EvalCmd("nodeId", Node.Not(Node.False))
      expectMsg(Node.EvalResp(true))
    }

    "correctly evaluate not not command" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.Not(Node.Not(Node.True)))
      expectMsg(Node.EvalResp(true))
    }

    "correctly evaluate or command" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.Or(Node.False, Node.False))
      expectMsg(Node.EvalResp(false))
      nodeActor ! Node.EvalCmd("nodeId", Node.Or(Node.True, Node.False))
      expectMsg(Node.EvalResp(true))
    }

    "correctly evaluate and command" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.And(Node.True, Node.True))
      expectMsg(Node.EvalResp(true))
      nodeActor ! Node.EvalCmd("nodeId", Node.And(Node.True, Node.False))
      expectMsg(Node.EvalResp(false))
    }

    "correctly evaluate positive InRelation command 1" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.In("some-relation", Node.True))
      graphRegionMock.expectMsg(Node.EvalCmd("b", Node.True))
      graphRegionMock.reply(Node.EvalResp(true))
      graphRegionMock.expectMsg(Node.EvalCmd("a", Node.True))
      graphRegionMock.reply(Node.EvalResp(true))
      expectMsg(Node.EvalResp(true))
    }

    "correctly evaluate positive InRelation command 2" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.In("some-relation", Node.True))
      graphRegionMock.expectMsg(Node.EvalCmd("b", Node.True))
      graphRegionMock.reply(Node.EvalResp(true))
      graphRegionMock.expectMsg(Node.EvalCmd("a", Node.True))
      graphRegionMock.reply(Node.EvalResp(false))
      expectMsg(Node.EvalResp(true))
    }

    "correctly evaluate negative InRelation command 1" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.In("some-relation", Node.True))
      graphRegionMock.expectMsg(Node.EvalCmd("b", Node.True))
      graphRegionMock.reply(Node.EvalResp(false))
      graphRegionMock.expectMsg(Node.EvalCmd("a", Node.True))
      graphRegionMock.reply(Node.EvalResp(false))
      expectMsg(Node.EvalResp(false))
    }

    "correctly evaluate negative InRelation command 2" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.In("not-relation", Node.True))
      graphRegionMock.expectNoMessage()
      expectMsg(Node.EvalResp(false))
    }

    "correctly evaluate positive OutRelation command 1" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.Out("some-relation", Node.True))
      graphRegionMock.expectMsg(Node.EvalCmd("b", Node.True))
      graphRegionMock.reply(Node.EvalResp(true))
      graphRegionMock.expectMsg(Node.EvalCmd("a", Node.True))
      graphRegionMock.reply(Node.EvalResp(true))
      expectMsg(Node.EvalResp(true))
    }

    "correctly evaluate positive OutRelation command 2" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.Out("some-relation", Node.True))
      graphRegionMock.expectMsg(Node.EvalCmd("b", Node.True))
      graphRegionMock.reply(Node.EvalResp(true))
      graphRegionMock.expectMsg(Node.EvalCmd("a", Node.True))
      graphRegionMock.reply(Node.EvalResp(false))
      expectMsg(Node.EvalResp(true))
    }

    "correctly evaluate negative OutRelation command 1" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.Out("some-relation", Node.True))
      graphRegionMock.expectMsg(Node.EvalCmd("b", Node.True))
      graphRegionMock.reply(Node.EvalResp(false))
      graphRegionMock.expectMsg(Node.EvalCmd("a", Node.True))
      graphRegionMock.reply(Node.EvalResp(false))
      expectMsg(Node.EvalResp(false))
    }

    "correctly evaluate negative OutRelation command 2" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.Out("not-relation", Node.True))
      graphRegionMock.expectNoMessage()
      expectMsg(Node.EvalResp(false))
    }

    "correctly evaluate long True expression" in {
      val expr = Node.Or(Node.And(Node.Eq("key", Node.AttrString("value")), Node.Not(Node.Eq("key", Node.AttrString("not value")))), Node.Out("some-relation", Node.True))
      nodeActor ! Node.EvalCmd("nodeId", expr)
      graphRegionMock.expectMsg(Node.EvalCmd("b", Node.True))
      graphRegionMock.reply(Node.EvalResp(false))
      graphRegionMock.expectMsg(Node.EvalCmd("a", Node.True))
      graphRegionMock.reply(Node.EvalResp(false))
      expectMsg(Node.EvalResp(true))
    }

    "correctly evaluate long False expression" in {
      val expr = Node.And(Node.And(Node.Eq("key", Node.AttrString("value")), Node.Not(Node.Eq("key", Node.AttrString("not value")))), Node.Out("some-relation", Node.True))
      nodeActor ! Node.EvalCmd("nodeId", expr)
      graphRegionMock.expectMsg(Node.EvalCmd("b", Node.True))
      graphRegionMock.reply(Node.EvalResp(false))
      graphRegionMock.expectMsg(Node.EvalCmd("a", Node.True))
      graphRegionMock.reply(Node.EvalResp(false))
      expectMsg(Node.EvalResp(false))
    }

    "correctly evaluate Label command" in {
      nodeActor ! Node.EvalCmd("nodeId", Node.Label("nodeLabel"))
      expectMsg(Node.EvalResp(true))

      nodeActor ! Node.EvalCmd("nodeId", Node.Label("wrong"))
      expectMsg(Node.EvalResp(false))
    }


    "correctly set Number attribute" in {
      nodeActor ! Node.AddAttrCmd("nodeId", "key1", Node.AttrNumber(1100))
      expectMsg(common.Ok)
      nodeActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeData(Map("key"->Node.AttrString("value"), "key1"-> Node.AttrNumber(1100)), "nodeLabel", Map("some-relation"->List("b", "a")), Map("some-relation"->List("b", "a")), Some("nextNodeId")))
    }

    "correctly update storage if condition is True" in {
      nodeActor ! Node.InitUpdCmd("nodeId","key1", Node.Prod(Node.Val(10), Node.Var("key1")), Node.True)
      expectMsg(common.Ok)
      // waiting because this method is async, it takes time to update storage
      Thread.sleep(100)
      nodeActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeData(Map("key"->Node.AttrString("value"), "key1"-> Node.AttrNumber(11000)), "nodeLabel", Map("some-relation"->List("b", "a")), Map("some-relation"->List("b", "a")), Some("nextNodeId")))
    }


    "correctly don't update storage if condition is False" in {
      nodeActor ! Node.InitUpdCmd("nodeId","key1", Node.Prod(Node.Val(10), Node.Var("key1")), Node.False)
      expectMsg(common.Ok)
      // waiting because this method is async, it takes time to update storage
      Thread.sleep(100)
      nodeActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeData(Map("key"->Node.AttrString("value"), "key1"-> Node.AttrNumber(11000)), "nodeLabel", Map("some-relation"->List("b", "a")), Map("some-relation"->List("b", "a")), Some("nextNodeId")))
    }

    "correctly remove Number attribute" in {
      nodeActor ! Node.DelAttrCmd("nodeId", "key1")
      expectMsg(common.Ok)
      nodeActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeData(Map("key"->Node.AttrString("value")), "nodeLabel", Map("some-relation"->List("b", "a")), Map("some-relation"->List("b", "a")), Some("nextNodeId")))
    }

    "correctly recover state after it's killed" in {
      system.stop(nodeActor)
      val otherActor: ActorRef = system.actorOf(Props(new Node() {
        override lazy val graphRegion: ActorRef = graphRegionMock.testActor
        override val persistenceId: String = "node-nodeId"
      }) , "otherNode")
      otherActor ! Node.GetNodeDataCmd("nodeId")
      expectMsg(Node.NodeData(Map("key"->Node.AttrString("value")), "nodeLabel", Map("some-relation"->List("b", "a")), Map("some-relation"->List("b", "a")), Some("nextNodeId")))
    }

  }

}
