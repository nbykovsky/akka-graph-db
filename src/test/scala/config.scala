import com.typesafe.config.{Config, ConfigFactory}

object config {

  val testingConf: Config = ConfigFactory.parseString("""
                                                akka {
                                                  loglevel = "INFO"

                                                  actor {
                                                    provider = "local"
                                                  }

                                                  persistence {
                                                    journal.plugin = "inmemory-journal"
                                                    snapshot-store.plugin = "inmemory-snapshot-store"
                                                  }
                                                }
                                             """)
}
