import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.cluster.sharding.ClusterSharding
import akka.persistence.{PersistentActor, RecoveryCompleted}
import akka.util.Timeout
import akka.pattern.{ask, pipe}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Success

/**
  NodeManager actor is used for tracking Nodes of particular type. For each type of node NodeManager actor is created.
  It stores the link to the head of the nodes chain

  For example:
  if we have 3 actors of type person, it will be organised as linked list (see Node.scala code). And to find head we
  will use NodeManager actor

    NodeManager -> ((head)person-2)->(person-1)->(person-0)

  Since node manager knows how to find actors, it's used for creating the new nodes

  if we want to add one more Node of type person in the example above it will become

    NodeManager -> ((head)person-3)->(person-2)->(person-1)->(person-0)

  Actors of NodeManager type are managed by cluster-sharding in labelRegion

  */
class NodeManager extends Actor with PersistentActor with ActorLogging{

  import context.dispatcher
  import NodeManager._

  implicit val timeout: Timeout = Timeout(5 seconds)

  // entity id in cluster-sharding is stored in self.path.name
  val persistenceId: String = "label-" + self.path.name

  val label: String = self.path.name

  // local actor for sending messages to nodes
  lazy val graphRegion: ActorRef = ClusterSharding(context.system).shardRegion("Graph")

  // mutable variables used only for recovery
  var tmpCounterForRecovery: Option[BigInt] = None
  var tmpHeadNodeForRecovery: Option[Node.NodeId] = None

  def receiveRecover: Receive = {
    case AddNodeEvt(nodeId, counter) =>
      tmpCounterForRecovery = Some(counter)
      tmpHeadNodeForRecovery = Some(nodeId)
    case RecoveryCompleted =>
      for {
        counter <- tmpCounterForRecovery
        head <- tmpHeadNodeForRecovery
      } yield context become processCommand(Some(head), counter + 1)
      tmpCounterForRecovery = None
      tmpHeadNodeForRecovery = None
  }

  def receiveCommand: Receive = processCommand(None, 0)

  // NodeManager generates nodeId!
  def getNodeId(label: String, counter: BigInt): String = s"$label-$counter"

  /**
    *
    * @param headNodeId - head on Nodes linked list
    * @param counter - increasing counter used for nodeId generation
    * @return
    */
  def processCommand(headNodeId: Option[Node.NodeId], counter: BigInt): Receive = {
    // add new node of lbl type
    case NodeManager.AddNodeCmd(lbl) =>
      val nodeId = getNodeId(lbl, counter)
      val snd = sender()
      val result = for {
        // first we set label to node. Node actor is getting created after this
        _ <- graphRegion ? Node.SetLabelCmd(nodeId, lbl)
        // second if this not the first node of this type we set nextNode to maintain liked list structure
        r2 <- headNodeId.map(graphRegion ? Node.SetNextNodeCmd(nodeId, _)).getOrElse(Future.successful(()))
      } yield r2
      result map (_ => NodePersist(snd, nodeId)) pipeTo self
    // persisting event, changing internal state and sending acknowledgment
    case NodeManager.NodePersist(snd, nodeId) =>
      persist(AddNodeEvt(nodeId, counter)) { _ =>
        context become processCommand(Some(nodeId), counter + 1)
        snd ! NodeResp(nodeId)
      }
    // fetch head Node from linked list
    case NodeManager.GetHead(_) if headNodeId.nonEmpty =>
      sender() ! NodeResp(headNodeId.get)
    // if nodes don't exists
    case NodeManager.GetHead(_) =>
      sender() ! common.Err
  }

}

object NodeManager {

  // label is used by cluster-sharding
  sealed trait Cmd {
    val label: Node.NodeLabel
  }

  case class AddNodeCmd(label: Node.NodeLabel) extends Cmd
  case class AddNodeEvt(headNodeId: Node.NodeId, counter: BigInt)
  case class NodePersist(snd: ActorRef, headNodeId: Node.NodeId)
  case class NodeResp(nodeId: Node.NodeId) extends common.SimpleOk
  case object NodeDoesntExists
  case class GetHead(label: Node.NodeLabel) extends Cmd
}