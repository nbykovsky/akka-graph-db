import akka.actor.{ActorRef, ActorSystem, Props}
import akka.cluster.sharding.ClusterSharding
import akka.util.Timeout
import akka.pattern.ask
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.language.postfixOps


/**
  Services provide internal API to talk to graph database

  */
class Service(implicit val system: ActorSystem) {

  implicit val timeout: Timeout = Timeout(5 seconds)
  //using akka's execution context to don't block thread
  implicit val ec: ExecutionContext = system.dispatcher

  // region for talking to NodeManager nodes
  val labelRegion: ActorRef = ClusterSharding(system).shardRegion("Label")

  // region for talking to Node nodes
  val graphRegion: ActorRef = ClusterSharding(system).shardRegion("Graph")

  /**
    * Adds Node to the database
    * @param nodeLabel - type of node which needs to be added
    * @return wrapped created nodeId
    */
  def addNodeSync(nodeLabel: Node.NodeLabel): Future[Node.NodeId] =
    (labelRegion ? NodeManager.AddNodeCmd(nodeLabel)).mapTo[NodeManager.NodeResp] map (_.nodeId )

  /**
    * Adds attribute to the existing node
    * @param nodeId target node
    * @param key attribute key
    * @param value attribute value (check values in Node.scala)
    * @return wrepped Unit
    */
  def addAttrSync(nodeId: Node.NodeId, key: Node.AttrKey, value: Node.AttrValue): Future[Unit] =
    (graphRegion ? Node.AddAttrCmd(nodeId,key,value)).mapTo[common.Ok.type ] map (_ => ())

  /**
    * Deletes key from the node (if exists)
    * @param nodeId - target node
    * @param key - key which needs to be deleted
    * @return
    */
  def delAttrSync(nodeId: Node.NodeId, key: Node.AttrKey): Future[Unit] =
    (graphRegion ? Node.DelAttrCmd(nodeId,key)).mapTo[common.Ok.type ] map (_ => ())

  /**
    * Fetches node data
    * @param nodeId target nodeId
    * @return wrapped node data
    */
  def getAllNodeAttrSync(nodeId: Node.NodeId): Future[Node.NodeData] =
    (graphRegion ? Node.GetNodeDataCmd(nodeId)).mapTo[Node.NodeData]

  /**
    * Connects two nodes. For each node adds inbound and outbound relations accordingly
    * @param from - source node
    * @param to - destination node
    * @param rel - relation type
    * @return wrapped Unit
    */
  def connectNodesSync(from: Node.NodeId, to: Node.NodeId, rel: Node.RelationType): Future[Unit] = {
    graphRegion ? Node.AddOutRelCmd(from, to, rel) zip graphRegion ? Node.AddInRelCmd(to, from, rel) flatMap {
      case (common.Ok, common.Ok) => Future.successful(())
      case _ => Future.failed(new Exception("Error during connecting Nodes"))
    }
  }

  /**
    * Fetches result of the qury
    * @param nodeLabel - requested node type
    * @param eval - condition
    * @return data from all the nodes of type nodeLabel which satisfy condition eval
    */
  def evalCommand(nodeLabel: Node.NodeLabel, eval: Node.Eval): Future[List[Node.InitEvalResp]] =
    (system.actorOf(Props(new Collector)) ? Collector.PostQuery(eval, nodeLabel)).mapTo[Collector.Result] map (_.data)


  /**
    * Updates set of nodes which satisfy conditions
    *
    * @param nodeLabel - type of nodes which supposed to be updated
    * @param key - key in storage which need to be updated (or created)
    * @param exp - math expression which will be evaluated taking into account node's internal data
    *            Example: "(key1 * 5) + key2" where key1 and key2 are keys of numeric variables. If keys are not there
    *            nothing will happen
    * @param condition - condition which needs to be satisfied for updating node
    * @return wrapped unit
    */
  def updateAttrAsync(nodeLabel: Node.NodeLabel, key: Node.AttrKey, exp: String, condition: Node.Eval): Future[Unit] = {
    parsers.exp.parse(exp) match {
      // trying to parse expression
      case Right(expression) =>
        val result = for {
          // fetching head nodeId
          NodeManager.NodeResp(nodeId) <- labelRegion ? NodeManager.GetHead(nodeLabel)
          // sending command
          r  <- graphRegion ? Node.InitUpdCmd(nodeId, key, expression, condition)
        } yield r
        result.mapTo[common.Ok.type].map(_ => ())
      // if expression is not correct don't do anything
      case Left(_) => Future.failed(new VerifyError("Unable to parse expression"))
    }

  }

}

