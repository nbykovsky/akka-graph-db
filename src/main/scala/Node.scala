import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.cluster.sharding.ClusterSharding
import akka.persistence.{PersistentActor, RecoveryCompleted}
import akka.pattern.{ask, pipe}
import akka.util.Timeout

import scala.concurrent.duration._
import scala.collection.mutable
import scala.concurrent.Future
import scala.language.postfixOps

/**
  Node actor represents a vertex of graph. Each node has type and all the nodes of given type represents linked list
  data structure (reference is stored in nextNode field). So if we need to select all the nodes who meet criteria, query
  is being sent to head node only and head node forwards it to downstream nodes.

  Each node has key-value storage with string keys which could store the following types:
  * AttrString - String
  * AttrNumber - Double number
  * AttrBool - Boolean
  * AttrList - list of attributes above

  Also node could have 2 kins of relations with other nodes: Inbound and Outbound. Each relation has string name.
  If node X has outbound relation with node Y, then node Y has inbound relation with node X

  Node supports the following high level operations:
  * Create a node of given type
  * Set Attribute of the node
  * Evaluate condition and if it's true send all the data to collector actor (which is part of query)
  * Evaluate condition and if it's true set attribute according to math expression (part of query)
  * Evaluate condition and send result to sender

  Nodes are managed by cluster-sharding which means that we don't need to know where node is located on cluster
  and send messages to graphRegion using nodeId

  */
class Node extends Actor with PersistentActor with ActorLogging {

  import Node._
  import context.dispatcher

  implicit val timeout: Timeout = Timeout(5 seconds)

  // since self.path.name is an entity(node) identifier the following definition of persistenceId should be safe
  val persistenceId: String = "node-" + self.path.name

  val nodeId: NodeId = self.path.name

  lazy val graphRegion: ActorRef = ClusterSharding(context.system).shardRegion("Graph")

  // following several mutable variables are used for recovery only
  val tmpStorageForRecovery: mutable.Map[AttrKey, AttrValue] = mutable.Map()
  var tmpLabelForRecovery: Option[NodeLabel] = None
  val tmpInForRecovery: mutable.Map[RelationType, List[NodeId]] = mutable.Map().withDefaultValue(List())
  val tmpOutForRecovery: mutable.Map[RelationType, List[NodeId]] = mutable.Map().withDefaultValue(List())
  var tmpNextNodeForRecovery: Option[NodeId] = None

  def receiveRecover: Receive = {
    case SetLabelEvt(l) =>
      tmpLabelForRecovery = Some(l)
    case AddAttrEvt(k, v) =>
      tmpStorageForRecovery += (k -> v)
    case DelAttrEvt(k) =>
      tmpStorageForRecovery -= k
    case AddInRelEvt(n, t) =>
      tmpInForRecovery(t) = n::tmpInForRecovery(t)
    case AddOutRelEvt(n, t) =>
      tmpOutForRecovery(t) = n::tmpOutForRecovery(t)
    case SetNextNodeEvt(n) =>
      tmpNextNodeForRecovery = Some(n)
    case RecoveryCompleted =>
      // if label is not set node doesn't exists
      tmpLabelForRecovery foreach { l =>
        context become processCommand(
          tmpStorageForRecovery.toMap,
          l,
          tmpInForRecovery.mapValues(_.toList).toMap.withDefaultValue(List()),
          tmpOutForRecovery.mapValues(_.toList).toMap.withDefaultValue(List()),
          tmpNextNodeForRecovery
        )
        // deleting object ot save some space
        tmpStorageForRecovery.clear()
        tmpLabelForRecovery = None
        tmpInForRecovery.clear()
        tmpOutForRecovery.clear()
      }
  }


  /**
    *
    * @param storage - key-value storage of attributes
    * @param label - type of Node
    * @param in - map(relation type -> nodeId) of inbound relations
    * @param out - map(relation type -> nodeId) of outbound relations
    * @param nextNode - link to the next node of this type
    * @return
    */
  def processCommand(
                      storage: Storage,
                      label:   NodeLabel,
                      in:      Edges,
                      out:     Edges,
                      nextNode: Option[NodeId]
                    ): Receive = {
    // second command which node should receive (only if a node of the current type already exists)
    // if this is the first node of given type this message will not come
    case SetNextNodeCmd(_, n) if nextNode.isEmpty =>
      persist(SetNextNodeEvt(n)) {_ =>
        context become processCommand(storage, label, in, out, Some(n))
        sender() ! common.Ok
      }
    // sets of updates attribute in storage map
    case AddAttrCmd(_, k, v) =>
      persist(AddAttrEvt(k, v)) { _ =>
        context become processCommand(storage + (k -> v), label, in, out, nextNode)
        sender() ! common.Ok
      }
    // deletes attribute from the storage map
    case DelAttrCmd(_, k) =>
      persist(DelAttrEvt(k)) {_ =>
        context become processCommand(storage - k, label, in, out, nextNode)
        sender() ! common.Ok
      }
    // adds inbound relation to the current node
    case AddInRelCmd(_, n, t) =>
      persist(AddInRelEvt(n, t)) { _ =>
        context become processCommand(storage, label, in + (t -> (n::in(t))), out, nextNode)
        sender() ! common.Ok
      }
    // adds outbound relation to the current node
    case AddOutRelCmd(_, n, t) =>
      persist(AddOutRelEvt(n, t)) { _ =>
        context become processCommand(storage, label, in,  out + (t -> (n::out(t))), nextNode)
        sender() ! common.Ok
      }
    // returns all the data from current node
    case GetNodeDataCmd(_) =>
      sender() ! NodeData(storage, label, in, out, nextNode)
    // accepts query and if condition is true sends result to the collector actor which present in path variable
    // after accepting message forwards it to downstream node of the same type.
    // this message is send only to the nodes which data is requested
    case InitEvalCmd(n, path, p) =>
      // actor which received node data if condition is true
      val collector = context.actorSelection(path)
      // extracting condition and sending it to self for evaluation
      self ? EvalCmd(n, p) map {
        // if condition is true sending data to the collector
        case EvalResp(true) => collector ! InitEvalResp(storage, nodeId, label)
        // in other cases don't do anything (probably it could be changed)
        case _ =>
      }
      // meanwhile forwarding this message to the next node in the chain
      nextNode foreach {nxt => graphRegion ! InitEvalCmd(nxt, path, p)}
      sender() ! common.Ok
    // command similar to InitEvalCmd but instead of sending result to the collector
    // if evaluates expression (exp) and puts it's value to the storage under key
    case InitUpdCmd(n, key, exp, eval) =>
      // value which we want to put to storage
      val value = parsers.evaluate(exp, storage collect {case (k, v:AttrNumber) => (k, v)})
      value foreach { v =>
        // check whether condition is true
        self ? EvalCmd(n, eval) map {
          case EvalResp(true) =>
            log.debug(s"Condition is true, storage key: $key value: ${storage.getOrElse(key,"")}->${AttrNumber(v)}")
            self ! AddAttrCmd(n, key, AttrNumber(v))
          case x =>
            log.debug(s"Condition is $x, don't updating anything")
        }
      }
      // forward request to the downstream node
      nextNode foreach {nxt => graphRegion !  InitUpdCmd(nxt, key, exp, eval)}
      sender() ! common.Ok
    // Request for predicate evaluation. If part of predicate couldn't be evaluated locally it gets sent to the
    // other node and result gets used once it's received
    case EvalCmd(n, predicate) =>
      val result = predicate match {
        // Always evaluates as true
        case True => Future.successful(true)
        // Always evaluates as false
        case False => Future.successful(false)
        // true if storage has key k with value v otherwise false
        case Eq(k, v) =>
          Future.successful(storage.get(k).contains(v))
        // true if pt predicate is false otherwise true
        case Not(pt) =>
          // unwrap pt predicate and send to itself for evaluation
          self ? EvalCmd(n, pt) map {case EvalResp(r) => !r}
        // true if any of p1 and p2 true
        case Or(p1, p2) =>
          // unwrap pt predicates and send to itself for evaluation
          self ? EvalCmd(n, p1) zip self ? EvalCmd(n, p2) map {
            case (EvalResp(r1), EvalResp(r2)) => r1 | r2
            // if calculation failed for some reason we return false
            case x =>
              log.warning(s"Got unexpected $x in OR operation")
              false
          }
        // true if both of p1 and p2 true
        case And(p1, p2) =>
          self ? EvalCmd(n, p1) zip self ? EvalCmd(n, p2) map {
            case (EvalResp(r1), EvalResp(r2)) => r1 && r2
            case x =>
              log.warning(s"Got unexpected $x in AND operation")
              false
          }
        // true if current node has inbound relation of type t and predicate pt is true for a node on different side of relation
        case In(t, pt) =>
          Future.sequence(in(t).map(graphRegion ? EvalCmd(_, pt))).map(_.collect {case EvalResp(true) => ()} nonEmpty)
        // true if current node has outbound relation of type t and predicate pt is true for a node on different side of relation
        case Out(t, pt) =>
          Future.sequence(out(t).map(graphRegion ? EvalCmd(_, pt))).map(_.collect {case EvalResp(true) => ()} nonEmpty)
        // returns true if current node has label l
        case Label(l) => Future.successful(l == label)
      }
      result map EvalResp pipeTo sender()
    case common.Ok =>
    case _ => sender() ! common.Err

  }

  def receiveCommand: Receive = {
    //creates node of given type
    //first command which node should receive
    case SetLabelCmd(_, l) =>
      persist(SetLabelEvt(l)) {_ =>
        context become processCommand(Map(), l, Map().withDefaultValue(List()), Map().withDefaultValue(List()), None)
        sender() ! common.Ok
      }
    //all the messages which are not SetLabelCmd - not expected
    case _ => sender() ! NodeDoesntExists
  }

}

object Node {

  type AttrKey = String
  type NodeLabel = String
  type NodeId = String
  type RelationType = String
  type Storage = Map[AttrKey, AttrValue]
  type Edges = Map[RelationType, List[NodeId]]


  // nodeId in this trait is used by cluster-sharding for routing messages
  sealed trait NodeCmd {
    val nodeId: NodeId
  }
  case class AddAttrCmd(nodeId: NodeId, key: AttrKey, value: AttrValue) extends NodeCmd
  case class AddAttrEvt(key: AttrKey, value: AttrValue)

  case class DelAttrCmd(nodeId: NodeId, key: AttrKey) extends NodeCmd
  case class DelAttrEvt(key: AttrKey)

  case class SetLabelCmd(nodeId: NodeId, label: NodeLabel) extends NodeCmd
  case class SetLabelEvt(label:NodeLabel)

  case class SetNextNodeCmd(nodeId: NodeId, targetNodeId: NodeId) extends NodeCmd
  case class SetNextNodeEvt(targetNodeId: NodeId)

  case class AddInRelCmd(nodeId: NodeId, targetNodeId: NodeId, RelationType: RelationType) extends NodeCmd
  case class AddInRelEvt(targetNodeId: NodeId, RelationType: RelationType)

  case class AddOutRelCmd(nodeId: NodeId, targetNodeId: NodeId, RelationType: RelationType) extends NodeCmd
  case class AddOutRelEvt(targetNodeId: NodeId, RelationType: RelationType)

  case class GetNodeDataCmd(nodeId: NodeId) extends NodeCmd
  case class NodeData(storage: Storage, label: NodeLabel, in: Edges, out: Edges, nextNode: Option[NodeId])

  case class InitEvalCmd(nodeId: NodeId, collector: Collector.ActorPath, eval: Eval) extends NodeCmd
  case class InitEvalResp(storage: Storage, nodeId: NodeId, label: NodeLabel)

  case class InitUpdCmd(nodeId: NodeId, key: AttrKey, exp: Exp, eval: Eval) extends NodeCmd

  case class EvalCmd(nodeId: NodeId, eval: Eval) extends NodeCmd
  case class EvalResp(resp: Boolean)

  case object NodeDoesntExists


  // description of commands in comments receiveCommand method
  sealed trait Eval
  case object True extends Eval
  case object False extends Eval
  case class Eq(key: AttrKey, value: AttrValue) extends Eval
  case class Or(p1: Eval, p2: Eval) extends Eval
  case class And(p1: Eval, p2: Eval) extends Eval
  case class Not(p: Eval) extends Eval
  case class In(relationType: RelationType, p: Eval) extends Eval
  case class Out(relationType: RelationType, p: Eval) extends Eval
  case class Label(value: NodeLabel) extends Eval

  // attributes which could be stored in key-value storage in node
  sealed trait AttrValue
  case class AttrString(value: String) extends AttrValue
  case class AttrNumber(value: Double) extends AttrValue
  case class AttrBool(value: Boolean) extends AttrValue
  case class AttrList(value: List[AttrValue]) extends AttrValue

  // expression which is used for updating a node
  sealed trait Exp
  case class Val(num: Double) extends Exp
  case class Var(key: Node.AttrKey) extends Exp
  case class Add(left: Exp, right: Exp) extends Exp
  case class Prod(left: Exp, right: Exp) extends Exp
  case class Sub(left: Exp, right: Exp) extends Exp



}
