import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.server.{Route, StandardRoute}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes

import scala.concurrent.ExecutionContext
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.directives.DebuggingDirectives
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.language.implicitConversions
import scala.util.{Failure, Success}

/**
 Routes for RESTful API for Graph DB
  */
class Routes(service: Service, log: LoggingAdapter)(implicit val ec: ExecutionContext) {

  import Routes._

  // always send external error if something if wrong
  def internalError(msg: String): StandardRoute = {
    log.error(msg)
    complete(StatusCodes.InternalServerError -> "Internal Error")
  }

  val ok: StandardRoute = complete(JsObject("message"->JsString("ok")))


  val route: Route =
    pathPrefix ("v1") {
      // creates new node
      post {
        path("add") {
          entity(as[Routes.AddNodeRequest]) { addNodeRequest =>
            onComplete(service.addNodeSync(addNodeRequest.label)) {
              case Success(nodeId) => complete(Routes.NodeIdEnv(nodeId))
              case Failure(e) => internalError(e.toString)
            }
          }
        }
      } ~
        // updates attribute on the existing noew
        post {
          path("attr") {
            entity(as[Routes.AddAttrRequest]) { req =>
              onComplete(service.addAttrSync(req.nodeId, req.key, req.value)) {
                case Failure(e) => internalError(e.toString)
                case Success(_) => ok
              }
            }
          }
        } ~
        // fetches all the data from node
        get {
          pathPrefix("node" / Segment) { nodeId: String =>
            onComplete(service.getAllNodeAttrSync(nodeId)) {
              case Failure(e) => internalError(e.toString)
              case Success(data) => complete(data)
            }
          }
        } ~
        // runs a query and returns the result
        post {
          path("eval") {
            entity(as[Routes.EvalRequest]) { req =>
              onComplete(service.evalCommand(req.label, req.eval)) {
                case Failure(e) => internalError(e.toString)
                case Success(items) => complete(items)
              }
            }
          }
        } ~
        // connects two nodes
        post {
          path("connect") {
            entity(as[Routes.ConnectNodesRequest]) { req =>
              onComplete(service.connectNodesSync(req.from, req.to, req.relation)) {
                case Failure(e) => internalError(e.toString)
                case Success(_) => ok
              }
            }
          }
        } ~
        // deletes arrrubute from the node
        post {
          path("delete") {
            entity(as[Routes.DelAttrRequest]) { req =>
              onComplete(service.delAttrSync(req.nodeId, req.key)) {
                case Failure(e) => internalError(e.toString)
                case Success(_) => ok
              }
            }
          }
        } ~
        // updates nodes which satisfy condition
        post {
          path("update") {
            entity(as[Routes.UpdateAttrRequest]) { req =>
              onComplete(service.updateAttrAsync(req.label, req.key, req.expression, req.condition)) {
                case Failure(e) => internalError(e.toString)
                case Success(_) => ok
              }
            }
          }
        }
    }

  val loggedRoute: Route = DebuggingDirectives.logRequestResult("Logger", Logging.InfoLevel)(route)
}


object Routes {

  case class AddNodeRequest(label: Node.NodeLabel)
  case class NodeIdEnv(nodeId: Node.NodeId)

  case class AddAttrRequest(nodeId: Node.NodeId, key: Node.AttrKey, value: Node.AttrValue)

  case class EvalRequest(label: Node.NodeLabel, eval: Node.Eval)
  case class EvalResponse(data: List[Node.InitEvalResp])

  case class ConnectNodesRequest(from: Node.NodeId, to: Node.NodeId, relation: Node.RelationType)

  case class DelAttrRequest(nodeId: Node.NodeId, key: Node.AttrKey)

  case class UpdateAttrRequest(label: Node.NodeLabel, key: Node.AttrKey, expression: String, condition: Node.Eval)

  val okResponse: JsObject = JsObject("message"->JsString("ok"))

  implicit def storage2JsValue(eval: Node.Storage):JsValue = JsObject(eval.mapValues(attrValueFormat.write))

  implicit val nodeIdFormat: RootJsonFormat[Routes.NodeIdEnv] = jsonFormat1(Routes.NodeIdEnv)
  implicit val addNodeRequestFormat: RootJsonFormat[Routes.AddNodeRequest] = jsonFormat1(Routes.AddNodeRequest)
  implicit val connectNodesFormat: RootJsonFormat[Routes.ConnectNodesRequest] = jsonFormat3(Routes.ConnectNodesRequest)
  implicit val nodeDataFormat: RootJsonFormat[Node.NodeData] = jsonFormat5(Node.NodeData)
  implicit val delAttrFormat: RootJsonFormat[Routes.DelAttrRequest] = jsonFormat2(Routes.DelAttrRequest)


  implicit object attrValueFormat extends RootJsonFormat[Node.AttrValue] {
    def write(attr: Node.AttrValue):JsValue = attr match {
      case Node.AttrString(value) => JsString(value)
      case Node.AttrBool(value) => JsBoolean(value)
      case Node.AttrNumber(value) => JsNumber(value)
      case Node.AttrList(value) => JsArray(value.map(write).toVector)
    }

    def read(value: JsValue): Node.AttrValue = value match {
      case JsString(str) => Node.AttrString(str)
      case JsBoolean(bool) => Node.AttrBool(bool)
      case JsNumber(num) => Node.AttrNumber(num.toDouble)
      case JsArray(vct) => Node.AttrList(vct.toList.map(read))
    }
  }

  implicit val addAttrRequestFormat: RootJsonFormat[Routes.AddAttrRequest] = jsonFormat3(Routes.AddAttrRequest)

  implicit def jsValue2string(v: JsValue): String = v match {
    case JsString(x) => x
    case y => y.toString()
  }

  implicit object evalReader extends RootJsonFormat[Node.Eval] {

    def write(eval: Node.Eval):JsValue = ???

    def read(value: JsValue): Node.Eval = value match {
      case JsBoolean(true) => Node.True
      case JsBoolean(false) => Node.False
      case JsObject(map) if map("type") == JsString("EQ") => Node.Eq(map("key"), attrValueFormat read map("value"))
      case JsObject(map) if map("type") == JsString("OR") => Node.Or(read(map("eval1")), read(map("eval2")))
      case JsObject(map) if map("type") == JsString("AND") => Node.And(read(map("eval1")), read(map("eval2")))
      case JsObject(map) if map("type") == JsString("NOT") => Node.Not(read(map("eval")))
      case JsObject(map) if map("type") == JsString("IN") => Node.In(map("kind"), read(map("eval")))
      case JsObject(map) if map("type") == JsString("OUT") => Node.Out(map("kind"), read(map("eval")))
      case JsObject(map) if map("type") == JsString("LABEL") => Node.Label(map("value"))
    }
  }

  implicit val evalRequestFormat: RootJsonFormat[Routes.EvalRequest] = jsonFormat2(Routes.EvalRequest)
  implicit val initEvalRespFormat: RootJsonFormat[Node.InitEvalResp] = jsonFormat3(Node.InitEvalResp)
  implicit val updAttrFormat: RootJsonFormat[Routes.UpdateAttrRequest] = jsonFormat4(Routes.UpdateAttrRequest)



}
