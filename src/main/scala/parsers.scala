import scala.language.postfixOps
import scala.util.parsing.combinator.JavaTokenParsers

/**
  Helper which does

  * parse math expression in update query
  * evaluate parsed expression

  */
object parsers {

  object exp extends JavaTokenParsers {

    /**
      * Recursively parses expression
      * @param str
      *            example: "(1 + 1) * (a + (b * (c + d)))"
      * @return recursive parsed expression or Error
      */
    def parse(str: String): Either[String, Node.Exp] =
      parseAll(expression, str) match {
        case Success(result, _) => Right(result)
        case failedOrIncomplete => Left(failedOrIncomplete.toString)
      }

    lazy val expression: Parser[Node.Exp] =  add | sub | prod | value | variable | factor

    private def variable: Parser[Node.Var] = ident  ^^ Node.Var

    private def value:Parser[Node.Val] = floatingPointNumber ^^ (x => Node.Val(x.toDouble))

    private def brackets: Parser[Node.Exp] =  "(" ~> expression <~ ")"

    private def factor:Parser[Node.Exp] = variable | value | brackets

    private def add: Parser[Node.Add] =  factor ~ "+" ~ expression ^^
      {case left ~ "+" ~ right => Node.Add(left,right)}

    private def prod: Parser[Node.Prod] =  factor ~ "*" ~ expression ^^
      {case left ~ "*" ~ right => Node.Prod(left,right)}

    private def sub: Parser[Node.Sub] =  factor ~ "-" ~ expression ^^
      {case left ~ "-" ~ right => Node.Sub(left,right)}

  }

  /**
    * Recursively evaluates expression
    *
    * @param exp parsed expression
    * @param dict filtered storage from Node (it includes only Node.AttrNumber attributes)
    * @return resulted number
    */
  def evaluate(exp: Node.Exp, dict: Map[Node.AttrKey, Node.AttrNumber]): Option[Double] = {

    // helper for evaluating binary operation
    def evalBin(op: (Double, Double) => Double, left: Node.Exp, right: Node.Exp): Option[Double] =
      evaluate(left, dict) zip evaluate(right, dict) map op.tupled headOption

    exp match {
      case Node.Val(v) => Some(v)
      case Node.Var(key) => dict.get(key).map(_.value)
      case Node.Add(left, right) => evalBin(_+_, left, right)
      case Node.Prod(left, right) => evalBin(_*_, left, right)
      case Node.Sub(left, right) => evalBin(_-_, left, right)
    }
  }

}
