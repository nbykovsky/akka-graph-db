import akka.actor.{ActorContext, ActorRef, ActorSystem, Address, ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider}


/**
  General perpose helpers
  */
object common {
  trait Resp
  trait SimpleOk extends Resp
  trait SimpleErr extends Resp

  case object Ok extends SimpleOk
  case object Err extends SimpleErr


  class RemoteAddressExtensionImpl(system: ExtendedActorSystem) extends Extension {
    def address: Address = system.provider.getDefaultAddress
  }

  object RemoteAddressExtension extends ExtensionId[RemoteAddressExtensionImpl]
    with ExtensionIdProvider {
    override def lookup: RemoteAddressExtension.type = RemoteAddressExtension
    override def createExtension(system: ExtendedActorSystem) = new RemoteAddressExtensionImpl(system)
    override def get(system: ActorSystem): RemoteAddressExtensionImpl = super.get(system)
  }

  // fetched remote actor's path in local context
  def remotePath(implicit context: ActorContext, self: ActorRef): String = {
    val remoteAddr: Address = common.RemoteAddressExtension(context.system).address
    self.path.toStringWithAddress(remoteAddr)
  }

}
