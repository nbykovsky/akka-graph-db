import akka.actor.{ActorSystem, Props}
import akka.cluster.sharding.{ClusterSharding, ClusterShardingSettings}
import com.typesafe.config.{Config, ConfigFactory}
import Node.NodeCmd
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.event.{Logging, LoggingAdapter}

import scala.concurrent.ExecutionContext

object Main {


  def main(args: Array[String]){

    val pat = "([0-9]+)".r

    // parsing arguments first integer node port, second - web port
    // in cluster mode first node must run on 2551
    val (portNode, portWeb) = args.toList match {
      case pat(n)::pat(w)::Nil => (n.toInt, w.toInt)
      case pat(n)::Nil => (n.toInt, 8080)
      case _ => (2551, 8080)
    }

    println(s"Node port: $portNode")
    println(s"Web port: $portWeb")

    val config = getConfig(portNode)

    implicit val system: ActorSystem = ActorSystem("ClusterSystem", config)
    implicit val ec: ExecutionContext = system.dispatcher
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    // starting graphRegion and labelRegion which are used by Node and NodeManager actors accordingly
    startRegions(100)

    val log: LoggingAdapter = Logging(system, "Logging")
    val service: Service = new Service
    val routes = new Routes(service, log)

    Http().bindAndHandle(routes.loggedRoute, "localhost", portWeb)
    println(s"Server online at http://localhost:8080/")

  }

  def startRegions(numberOfShards: Int)(implicit system: ActorSystem): Unit = {
    ClusterSharding(system).start(
      typeName = "Graph",
      entityProps = Props(new Node()),
      settings = ClusterShardingSettings(system),
      extractEntityId = {case msg: NodeCmd => (msg.nodeId, msg)},
      extractShardId = {case msg: NodeCmd => (msg.nodeId.hashCode % numberOfShards).toString}
    )

    ClusterSharding(system).start(
      typeName = "Label",
      entityProps = Props(new NodeManager()),
      settings = ClusterShardingSettings(system),
      extractEntityId = {case msg: NodeManager.Cmd => (msg.label, msg)},
      extractShardId = {case msg: NodeManager.Cmd => (msg.label.hashCode % numberOfShards).toString}
    )
  }

  def getConfig(portNode: Int): Config = ConfigFactory.parseString( s"akka.remote.artery.canonical.port=$portNode")
    .withFallback(ConfigFactory.parseString("akka.cluster.roles = [backend]"))
    .withFallback(ConfigFactory.load())

}
