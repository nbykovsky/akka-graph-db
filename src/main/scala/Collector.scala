import akka.actor.{Actor, ActorLogging, ActorRef, Address}
import akka.cluster.sharding.ClusterSharding
import akka.util.Timeout
import akka.pattern.ask

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Success
import scala.collection.mutable


/**
  Collector actor is created for each database query. It collects the data from Node actors and sends it to initial
  sender after timeout

  */
class Collector extends Actor with ActorLogging{

  implicit val timeout: Timeout = Timeout(5 seconds)
  import context.dispatcher

  // put it here to be able to override during unittesting
  val remotePath: String = common.remotePath

  // again, have lazy only for unittesting
  lazy val graphRegion: ActorRef = ClusterSharding(context.system).shardRegion("Graph")
  lazy val labelRegion: ActorRef = ClusterSharding(context.system).shardRegion("Label")

  // mutable accumulator of incoming data
  var acc: mutable.ArrayBuffer[Node.InitEvalResp] = mutable.ArrayBuffer.empty

  // here will be stored sender of initial request
  var snd: Option[ActorRef] = None

  def receive: Receive = {
    // message which initiates query of actor l with condition e
    case Collector.PostQuery(e, l) =>
      snd = Some(sender())
      val result = for {
         // first we need to know address of head Node of type l
         NodeManager.NodeResp(nodeId) <- labelRegion ? NodeManager.GetHead(l)
         // second we send query to the Head Node. It will evaluate it and forward the the downstream nodes
         r <- graphRegion ? Node.InitEvalCmd(nodeId, remotePath, e)
      } yield r
      result onComplete {
        // once query is accepted it will wait for sometime before produce the result
        // todo: timeout value should be send by requestor
        case Success(_) => context.system.scheduler.scheduleOnce(1 second, self, Collector.GetResult)
        case _ => snd foreach {_ ! common.Err}
      }
    // waiting is over it's time to produce result
    case Collector.GetResult =>
      snd foreach  {_ ! Collector.Result(acc.toList)}
    // collecting information from the nodes
    case item: Node.InitEvalResp =>
      acc += item
  }
}

case object Collector {

  type ActorPath = String

  case class PostQuery(eval: Node.Eval, label: Node.NodeLabel)
  case object GetResult
  case class Result(data: List[Node.InitEvalResp]) extends common.SimpleOk
}

